﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MovingObjectsGenerator : MonoBehaviour
{
    [SerializeField] List<GameObject> objPrefabs = new List<GameObject>();
    [SerializeField] int objCount;

    [SerializeField] float heightObjSpawn;

    // z-Coordinates for create objects
    [SerializeField] int leftZ;
    [SerializeField] int rightZ;

    // interval parameters of object's appearence
    [SerializeField] int minInterval;
    [SerializeField] int maxInterval;

    private enum Direction { Left = -1, Right = 1 }
    private Direction direction;

    private float tripSpeed;
    private float interval;
    private float elapsedTime;

    private ObjectsPool objPool = new ObjectsPool();

    void Start()
    {
        direction = Random.value < 0.5f ? Direction.Left : Direction.Right;
        tripSpeed = Random.Range(3, 6);

        interval = 2;
        elapsedTime = 0;

        for (int i = 0; i < objCount; i++)
        {
            CreateObj();
        }
    }

    void Update()
    {
        elapsedTime += Time.deltaTime;
        if (elapsedTime > interval)
        {
            interval = Random.Range(minInterval, maxInterval);
            elapsedTime = 0;

            GameObject newObj = objPool.GetPoolObject();
            if (newObj != null)
            {
                newObj.SetActive(true);
                SetStartObjPosition(newObj);
                StartCoroutine(MoveObj(newObj));
            }
        }
    }

    private void OnDisable()
    {
        objPool.DisableObjects();
    }

    void CreateObj()
    {
        int randomObj = Random.Range(0, objPrefabs.Count);
        GameObject obj = Instantiate(objPrefabs[randomObj]);

        obj.SetActive(false);
        objPool.AddToPool(obj);
    }

    void SetStartObjPosition(GameObject obj)
    {
        Vector3 startPosition = new Vector3(transform.position.x, transform.position.y + heightObjSpawn, direction == Direction.Left ? rightZ : leftZ);
        Quaternion startRotation = direction == Direction.Right ? Quaternion.identity : new Quaternion(0, 180, 0, 0);

        obj.transform.position = startPosition;
        obj.transform.rotation = startRotation;
    }

    IEnumerator MoveObj(GameObject obj)
    {
        float endValue = direction == Direction.Left ? leftZ : rightZ;
        Tween myTween = obj.transform.DOMoveZ(endValue, tripSpeed).SetEase(Ease.Linear);

        yield return myTween.WaitForCompletion();
        obj.SetActive(false);
    }
}
