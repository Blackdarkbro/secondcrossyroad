﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class MovePlayerController : MonoBehaviour, IDragHandler, IBeginDragHandler
{
    [SerializeField] GameObject player;

    [SerializeField] float durationMove = 0.2f;
    [SerializeField] float durationRotate = 0.2f;
    [SerializeField] float jumpPower = 1f;

    [SerializeField] AudioSource jumpSound;

    public LayerMask untouchable;

    // Alloed borders for moving
    [SerializeField] int minZ = -6;
    [SerializeField] int maxZ = 8;

    private int step = 2;
    private float maxReachedDistance = 0;
    private int backStepCount = 4;

    private Vector3 playerPosition;
    private Player playerScript;

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (!playerScript.canMove) return;

        if (Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))
        {
            // right & left moving
            if (eventData.delta.x > 0)
            {
                Vector3 newPosition = new Vector3(Mathf.RoundToInt(playerPosition.x), playerPosition.y, Mathf.RoundToInt(playerPosition.z + step));
                MoveCharacter(newPosition, 90);
            }
            else
            {
                Vector3 newPosition = new Vector3(Mathf.RoundToInt(playerPosition.x), playerPosition.y, Mathf.RoundToInt(playerPosition.z - step));
                MoveCharacter(newPosition, -90);
            }
        }
        else
        {
            // top & bottom moving
            if (eventData.delta.y > 0)
            {
                Vector3 newPosition = new Vector3(Mathf.RoundToInt(playerPosition.x - step), playerPosition.y, Mathf.RoundToInt(playerPosition.z));
                MoveCharacter(newPosition, 0);
            }
            else
            {
                Vector3 newPosition = new Vector3(Mathf.RoundToInt(playerPosition.x) + step, playerPosition.y, Mathf.RoundToInt(playerPosition.z));
                MoveCharacter(newPosition, 180);
            }
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        
    }
    void Start()
    {
        playerScript = player.GetComponent<Player>();
    }

    void Update()
    {
        playerPosition = player.transform.position;

        if( playerScript.canMove)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                Vector3 newPosition = new Vector3(Mathf.RoundToInt(playerPosition.x - step), playerPosition.y, Mathf.RoundToInt(playerPosition.z));
                MoveCharacter(newPosition, 0);
            }
            if (Input.GetKeyDown(KeyCode.S))
            {
                Vector3 newPosition = new Vector3(Mathf.RoundToInt(playerPosition.x) + step, playerPosition.y, Mathf.RoundToInt(playerPosition.z));
                MoveCharacter(newPosition, 180);
            }
            if (Input.GetKeyDown(KeyCode.A))
            {
                Vector3 newPosition = new Vector3(Mathf.RoundToInt(playerPosition.x), playerPosition.y, Mathf.RoundToInt(playerPosition.z - step));
                MoveCharacter(newPosition, -90);
            }
            if (Input.GetKeyDown(KeyCode.D))
            {
                Vector3 newPosition = new Vector3(Mathf.RoundToInt(playerPosition.x), playerPosition.y, Mathf.RoundToInt(playerPosition.z + step));
                MoveCharacter(newPosition, 90);
            }
        }

    }

    private void MoveCharacter(Vector3 newPos, float rotateAngle)
    {
        player.transform.DORotate(new Vector3(0, rotateAngle, 0), durationRotate);

        maxReachedDistance = Mathf.Min(playerPosition.x, maxReachedDistance);
        if (playerPosition.x >= 2) maxReachedDistance = 0;

        if (Physics.CheckSphere(newPos, 0.1f, untouchable) || newPos.z > maxZ || newPos.z < minZ || newPos.x - backStepCount > maxReachedDistance) return;
        jumpSound.Play();
        StartCoroutine(Jump(newPos));
    }
    
    IEnumerator Jump(Vector3 newPos)
    {
        Tween myTween = player.transform.DOJump(newPos, jumpPower, 1, durationMove);
        yield return myTween.WaitForCompletion();
    }

  
}
