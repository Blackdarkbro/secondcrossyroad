﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGenerator : MonoBehaviour
{
    [SerializeField] GameObject tree;

    [SerializeField] int maxTreeCount;
    [SerializeField] int minTreeCount;

    // z-Coordinates for create trees
    [SerializeField] int leftZ;
    [SerializeField] int rightZ;

    private int treesCount;
    private ObjectsPool treesPool = new ObjectsPool();

    void Start()
    {
        treesCount = Random.Range(minTreeCount, maxTreeCount);

        for (int i = 0; i < treesCount; i++)
        {

            Vector3 treePos = GetTreePosition();
            treesPool.AddToPool(Instantiate(tree, treePos, Quaternion.identity));
        }
    }

    private Vector3 GetTreePosition()
    {
        float zPos = Random.Range(leftZ, rightZ);
        zPos = zPos % 2 == 0 ? zPos : zPos + 1;

        return new Vector3(transform.position.x, transform.position.y, zPos);
    }

    private void OnDisable()
    {
        treesPool.DisableObjects();
    }
    private void OnEnable()
    {
        foreach (var item in treesPool.objects)
        {
            GameObject tree = treesPool.GetPoolObject();
            if (tree != null)
            {
                tree.transform.position = GetTreePosition();
                tree.SetActive(true);
            }
        }
    }
}
