﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinGenerator : MonoBehaviour
{
    [SerializeField] GameObject coinPrefab;

    [SerializeField] int minZ = -6, maxZ = 8;

    private GameObject currentCoin;

    private void OnEnable()
    {
        bool chance = Random.value < 0.3f ? true : false;

        if (chance)
        {
            currentCoin = Instantiate(coinPrefab, new Vector3(transform.position.x, transform.position.y + 0.75f, Random.Range(minZ, maxZ)), coinPrefab.transform.rotation);

        }
    }
    private void OnDisable()
    {
        Destroy(currentCoin);
    }
}
