﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class Player : MonoBehaviour
{
    public bool canMove = false;
    
    [SerializeField] AudioSource splashSound;
    [SerializeField] AudioSource crashSound;
    [SerializeField] AudioSource looseSound;


    private int score = 0;
    private int scoreRecord = 0;
    private int coins = 0;

    private GameStatController gameStatController;

    void Start()
    {
        gameStatController = GameObject.Find("GameStatController").GetComponent<GameStatController>();
    }

    void Update()
    {   
        score = Mathf.Min(score, (int)transform.position.x / 2);
        scoreRecord = Mathf.Min(score, scoreRecord);

        gameStatController.record = scoreRecord;
        gameStatController.score = score;

        if (transform.position.y < -0.2f)
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        looseSound.Play();
        canMove = false;
        gameStatController.GameOver();
    }
    public void Reset()
    {
        transform.position = new Vector3(4, 0.6f, 0);
        transform.rotation = Quaternion.identity;
        score = 0;
    }

    private void OnCollisionEnter(Collision collision)
    {
        // crash from car
        if (collision.gameObject.tag == "Car")
        {
            crashSound.Play();
            GameOver();
        }
        // fall to water
        if (collision.gameObject.tag == "Water")
        {
            splashSound.Play();
            GameOver();
        }

        if (collision.gameObject.tag == "Coin")
        {
            Destroy(collision.gameObject);
            gameStatController.coins = ++coins;
        }
    }

   
}
