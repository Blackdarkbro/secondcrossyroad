﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsPool
{
    public List<GameObject> objects = new List<GameObject>();

    public void AddToPool(GameObject obj)
    {
        objects.Add(obj);
    }

    public GameObject GetPoolObject()
    {
        for (int i = 0; i < objects.Count; i++)
        {
            if (!objects[i].activeSelf)
            {
                return objects[i];
            }
        }

        return null;
    }

    public void DisableObjects()
    {
        foreach (var item in objects)
        {
            item.SetActive(false);
        }
    }
    public void ClearPool()
    {
        objects.RemoveRange(0, objects.Count);
    }
}

