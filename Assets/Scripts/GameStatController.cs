﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameStatController : MonoBehaviour
{
    [SerializeField] GameObject mainMenuCanvas;
    [SerializeField] GameObject playCanvas;
    [SerializeField] GameObject gameOverCanvas;

    [SerializeField] Text playScore;
    [SerializeField] Text gameOverScore;

    [SerializeField] Text playRecordScore;
    [SerializeField] Text gameOverRecordScore;

    [SerializeField] Text coinsScore;
    [SerializeField] Text gameOverCoinsScore;

    public int score;
    public int record;
    public int coins;

    private enum State { MainMenu, GameOver, Play }
    private State state;

    private GameObject currentCanvas;

    void Start()
    {
        currentCanvas = null;
        MainMenu();
    }

    // Update is called once per frame
    void Update()
    {
        coinsScore.text = coins.ToString();
        playScore.text = "Score:" + Mathf.Abs(score).ToString();
        playRecordScore.text = "Record: " + Mathf.Abs(record).ToString();

        if (state == State.MainMenu)
        {
            if (Input.GetButtonDown("Cancel"))
            {
                Application.Quit();
            }
            else if (Input.anyKeyDown)
            {
                Play();
            }
        }
        else if (state == State.GameOver)
        {
            if (Input.anyKeyDown)
            {
                MainMenu();
            }
        }
    }

    public void MainMenu()
    {
        CurrentCanvas = mainMenuCanvas;
        state = State.MainMenu;

        GameObject.FindGameObjectWithTag("Player").SendMessage("Reset");
        GameObject.FindGameObjectWithTag("MainCamera").SendMessage("Reset");
        GameObject.FindGameObjectWithTag("StripGenerator").SendMessage("Reset");
    }

    public void Play()
    {
        CurrentCanvas = playCanvas;
        state = State.Play;

        GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().canMove = true;
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<FollowPlayer>().canMove = true;
    }

    public void GameOver()
    {
        CurrentCanvas = gameOverCanvas;
        state = State.GameOver;

        gameOverScore.text = "Your score: " + Mathf.Abs(score).ToString();
        gameOverRecordScore.text = "Your record: " + Mathf.Abs(record).ToString();
        gameOverCoinsScore.text = "Coins: " + coins.ToString();

        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<FollowPlayer>().canMove = false;
    }

    private GameObject CurrentCanvas
    {
        get
        {
            return currentCanvas;
        }
        set
        {
            if (currentCanvas != null)
            {
                currentCanvas.SetActive(false);
            }
            currentCanvas = value;
            currentCanvas.SetActive(true);
        }
    }
}
