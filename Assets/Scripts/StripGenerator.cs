﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StripGenerator : MonoBehaviour
{
    [SerializeField] int posX = 0;
    [SerializeField] int poolSize;

    [SerializeField] GameObject[] stripsPrefabs;

    private GameObject player;

    private int stripsCount;
    private ObjectsPool stripsPool = new ObjectsPool();

    private int lineBehind = 11;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        for (int i = 0; i < poolSize; i++)
        {
            CreateStrip();
        }
    }

    // Update is called once per frame
    void Update()
    {
        int playerPositionX = Mathf.RoundToInt(player.transform.position.x);

        foreach (var line in stripsPool.objects) 
        {
            if (line.transform.position.x > playerPositionX + lineBehind * 2 - 8)
            {
                line.SetActive(false);

                GameObject newStrip = stripsPool.GetPoolObject();
                if (newStrip != null)
                {
                    newStrip.SetActive(true);
                    newStrip.transform.position = new Vector3(posX, 0, 0);
                    posX -= 2;
                }
            }
        }
    }

    void CreateStrip()
    {
        int randomTrip = Random.Range(0, stripsPrefabs.Length);
        GameObject trip = Instantiate(stripsPrefabs[randomTrip], new Vector3(posX, 0, 0), Quaternion.identity);

        trip.SetActive(true);

        stripsPool.AddToPool(trip);

        posX -= 2;
    }

    IEnumerator MoveObj(GameObject obj)
    {
        yield return new WaitForSeconds(2f);
        obj.SetActive(false);
    }

    public void Reset()
    {
        foreach (var line in stripsPool.objects)
        {
            Destroy(line);
        }

        stripsPool.ClearPool();
        posX = 0;

        for (int i = 0; i < poolSize; i++)
        {
            CreateStrip();
        }
    }
}
